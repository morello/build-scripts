#!/usr/bin/env bash

# Copyright (c) 2021-2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

do_build()
{
    fw_version=$(fetch_fw_version "$WORKSPACE_DIR/bsp/scp" "SCP")
    version_numbers=$(fw_version_extract "$fw_version")
    read -r major_version minor_version patch_version <<< "$version_numbers"
    local build_path="output/$PLATFORM/intermediates/scp/cmake-build/product/morello"
    for scp_fw in "scp_ramfw_$PLATFORM" "mcp_ramfw_$PLATFORM" ; do
        local makeopts=(
            -S "bsp/scp"
            -B "$build_path/$scp_fw"
            -DCMAKE_ASM_COMPILER="${GCC_ARM32_PREFIX}gcc"
            -DCMAKE_C_COMPILER="${GCC_ARM32_PREFIX}gcc"
            -DSCP_ENABLE_DEBUGGER="$SCP_CLI_DEBUGGER"
            -DSCP_FIRMWARE_SOURCE_DIR:PATH="morello/$scp_fw"
            -DSCP_TOOLCHAIN:STRING="GNU"
            -DBUILD_VERSION_DESCRIBE_STRING:STRING="${fw_version}"
            -DBUILD_VERSION_MAJOR:INT="$major_version"
            -DBUILD_VERSION_MINOR:INT="$minor_version"
            -DBUILD_VERSION_PATCH:INT="$patch_version"
            -DDISABLE_CPPCHECK="$SCP_DISABLE_CPPCHECK"
        )

        if [[ "$scp_fw" == "scp_ramfw_soc" ]] ; then
           makeopts+=( "-DSCP_MORELLO_SENSOR_LIB_PATH=$WORKSPACE_DIR/bsp/board-firmware/LIB/sensor.a" )
        fi

        case "${SCP_BUILD_MODE,,}" in
        ("release") makeopts+=( "-DCMAKE_BUILD_TYPE=Release" ) ;;
        ("debug") makeopts+=( "-DCMAKE_BUILD_TYPE=Debug" ) ;;
        (*) die "Unsupported value for SCP_BUILD_MODE: $SCP_BUILD_MODE"
        esac

        if [[ -v "SCP_LOG_LEVEL" ]] ; then
            makeopts+=( "-DSCP_LOG_LEVEL=${SCP_LOG_LEVEL^^}" )
        fi

        cmake "${makeopts[@]}"
        cmake --build "$build_path/$scp_fw" --parallel "$PARALLELISM"
    done

    mkdir -p "$PLATFORM_OUT_DIR/intermediates"
    cp "$build_path/scp_ramfw_$PLATFORM/bin/morello-$PLATFORM-bl2.bin"     "$PLATFORM_OUT_DIR/intermediates/scp-ram.bin"
    cp "$build_path/mcp_ramfw_$PLATFORM/bin/morello-$PLATFORM-mcp-bl2.bin" "$PLATFORM_OUT_DIR/intermediates/mcp-ram.bin"
}

do_clean()
{
    rm -rf \
        "$PLATFORM_OUT_DIR/intermediates/scp/cmake-build/product/morello" \
        "$PLATFORM_OUT_DIR/intermediates/mcp-ram.bin" \
        "$PLATFORM_OUT_DIR/intermediates/scp-ram.bin"
}

source "$(dirname ${BASH_SOURCE[0]})/framework.sh"
