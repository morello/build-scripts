#!/usr/bin/env bash

# Copyright (c) 2021, 2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

do_build()
{
    if [[ "${FILESYSTEM%%-*}" != "android" ]] ; then
        echo "No Android to build for file-system \"$FILESYSTEM\""
        return 0
    fi

    local -r android_fs_type="${FILESYSTEM#android-}"
    local lunch_tgt="morello_${android_fs_type}-eng"
    case "$android_fs_type" in
    ("nano"*)
        # skip metalava checks (which take a lot of time and RAM) which is safe
        # to skip for nano
        export WITHOUT_CHECK_API=1
        ;;
    esac
    local -r lunch_tgt
    cd "$WORKSPACE_DIR/android"

    echo "Starting android source build for lunch target $lunch_tgt"
    with_default_shell_opts source build/envsetup.sh
    # vendorsetup.sh (sourced by envsetup.sh) report status with this variable
    [[ "$MORELLO_VENDORSETUP_STATUS" -eq 0 ]] || die "Android build error"
    with_default_shell_opts lunch "$lunch_tgt" || die "Android build error"
    with_default_shell_opts m -j $PARALLELISM || die "Android build error"
}

do_clean() {
    rm -rf "android/out"
}

source "$(dirname ${BASH_SOURCE[0]})/framework.sh"
