#!/usr/bin/env bash

# Copyright (c) 2021-2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

readonly DEFAULT_SHELL_OPTS="$(set +o)"
set -E
set -o pipefail
set -u

# excute a command with the default shell options (mostly useful with external
# shell functions)
with_default_shell_opts() {
    local -r original_opts="$(set +o)"
    eval "$DEFAULT_SHELL_OPTS"
    "$@"
    local -r i=$?
    eval "$original_opts"
    return $i
}

print_trace() {
    local -a lineno func file
    local len_lineno=0 len_func=0 len_file=0
    local i
    local callinfo
    for ((i=0; ; i++)) ; do
        callinfo="$(caller $((i+1)))" || break

        lineno+=( "${callinfo%% *}" )
        [[ ${#lineno[i]} -lt $len_lineno ]] || len_lineno=${#lineno[i]}
        callinfo="${callinfo#* }"

        func+=( "${callinfo%% *}" )
        [[ ${#func[i]} -lt $len_func ]] || len_func=${#func[i]}
        callinfo="${callinfo#* }"

        file+=( "$callinfo" )
        [[ ${#file[i]} -lt $len_file ]] || len_file=${#file[i]}
    done
    local -r depth="$i"

    local -r fmt_str="%-${len_func}s  %-${len_file}s  %-${len_lineno}s\n"
    printf "$BOLD$fmt_str$NORMAL" "func" "file" "line"
    for ((i=0; i<depth ; i++)) ; do
        printf "$fmt_str" "${func[i]}" "${file[i]}" "${lineno[i]}"
    done
}

handle_error () {
    local -r exit_code=$?
    {
        error_echo "Command terminated with a non-zero code!"
        echo "PLATFORM   = ${PLATFORM:-}"
        echo "FILESYSTEM = ${FILESYSTEM:-}"
        echo "WD         = $PWD"
        echo "EXIT CODE  = $exit_code"
        echo ""
        echo "Build-script call trace:"
        print_trace
    } >&2
    exit 1
}

if [[ -t 1 ]] ; then
    BOLD="\e[1m"
    NORMAL="\e[0m"
    RED="\e[31m"
    GREEN="\e[32m"
    YELLOW="\e[33m"
    BLUE="\e[94m"
    CYAN="\e[36m"
else
    BOLD=""
    NORMAL=""
    RED=""
    GREEN=""
    YELLOW=""
    BLUE=""
    CYAN=""
fi
readonly BOLD NORMAL RED GREEN YELLOW BLUE CYAN

trap handle_error ERR

error_echo() {
    echo -e "$BOLD${RED}ERROR:$NORMAL$RED $*$NORMAL" >&2
}

die() {
    error_echo "$*"
    exit 1
}

in_haystack() {
    local -r needle="$1" ; shift
    local haystack
    for haystack in "$@" ; do
        [[ $needle != $haystack ]] || return 0
    done
    return 1
}

esp_compress() {
    case "${COMPRESS_ESP^^}" in
    ("GZIP") gzip -v ;;
    ("NONE") cat ; return ;;
    (*) die "Unsupported COMPRESS_ESP config value: $COMPRESS_ESP" ;;
    esac
}

# newer_ctime COMPARE_FILE CHECK_FILE...
#
# Returns TRUE if any of...
# 1. the input do not exist the return value is TRUE.
# 2. CHECK_FILE... has a newer ctime than COMPARE_FILE.
newer_ctime() {
    local f
    for f in "$@" ; do
        [[ -e "$f" ]] || return 0
    done

    local -r state_file="$1" ; shift
    [[ -n $(find "$@" -cnewer "$state_file" -print -quit) ]]
}

fetch_fw_version() {
    cd "$1"
    branch_info="unknown"
    branch_info_master=""
    local_commit_n=""
    local_commit_n_master=""
    add_str=""
    commit_id_str=$(git log --pretty=%h -1)
    commit_id=${commit_id_str:0:8}

    # Fetch description matching only master and morello release branches
    # If based on a release, set branch as release version
    # If based on master, set branch as master
    # Otherwise, set branch as unknown
    if [[ $(git describe --all --always --match "*morello/release-[0-9]*\.[0-9]*" | grep "morello/release-") ]] ; then
        branch_info=$(git describe --all --long --match "*morello/release-[0-9]*\.[0-9.]*")
        local_commit_n=$(grep -oe "-[0-9]*-" <<< "${branch_info}" | grep -oe "[0-9]*")
        branch_info=$( grep -oe "[0-9]*\.[0-9]*\.*[0-9]*" <<< "${branch_info}")
    fi
    if [[ $(git describe --all --always --match "*morello/master" | grep "morello/master") ]] ; then
        branch_info_master=$(git describe --all --long --match "*morello/master")
        local_commit_n_master=$(grep -oe "-[0-9]*-" <<< "${branch_info_master}" | grep -oe "[0-9]*")
        # If based on master and not based on release, or more recent on
        # master compared to release, then set branch as master
        if [[ ("$local_commit_n" == "" || "$local_commit_n" -gt "$local_commit_n_master") ]] ; then
            local_commit_n="$local_commit_n_master"
            branch_info="master"
        fi
    fi

    if [[ "$local_commit_n" == "" ]] ; then
        local_commit_n="0"
    fi

    # staged and unstaged changes
    if [[ $(git diff HEAD --stat) != '' || "$local_commit_n" != "0" ]] ; then
        add_str="-dirty"
    fi

    cd "$WORKSPACE_DIR"
    echo "$2 Version: $branch_info$add_str commit: $commit_id" >&2
    echo "${branch_info}${add_str}_${commit_id}"
}

# Extract major, minor, and patch/flags values from version string
fw_version_extract() {
    # Truncate the commit_id from the branch_info
    branch_info="${1%%_*}"
    major=0
    minor=0
    patch=0
    flags=0
    if [[ "$branch_info" == *"dirty"* ]] ; then
        flags=$((flags | (1 << 7)))
        #Truncate dirty flag from the branch_info
        branch_info="${1%%-*}"
    fi
    if [[ "$branch_info" == *"master"* ]] ; then
        flags=$((flags | (1 << 6)))
    fi
    if [[ "$branch_info" == *"."* ]] ; then
        local IFS="."; read -r major minor patch <<< "$branch_info"
        if [[ -z "$patch" ]] ; then
            patch=0
        fi
    fi
    patch=$(( (patch << 8) | (flags) ))
    echo "$major $minor $patch"
}

readonly DO_DESC_all="alias for \"clean build\""
do_all() {
    cd "$WORKSPACE_DIR"
    do_clean
    cd "$WORKSPACE_DIR"
    do_build
}

if [[ -v PARALLELISM ]] ; then
    echo "PARALLELISM set in environment to $PARALLELISM, not overridden."
else
    PARALLELISM=`getconf _NPROCESSORS_ONLN`
fi
readonly PARALLELISM

# custom text set by the component script
readonly DO_DESC_build
readonly DO_DESC_clean

readonly SCRIPT_DIR="$(realpath --no-symlinks "$(dirname "${BASH_SOURCE[0]}")")"
readonly WORKSPACE_DIR="$(realpath --no-symlinks "$SCRIPT_DIR/..")"

readonly LLVM_PATH="$WORKSPACE_DIR/tools/clang_linux_x86/bin"
readonly GCC_ARM64_PREFIX="$WORKSPACE_DIR/tools/arm_linux_gcc/bin/aarch64-none-linux-gnu-"
readonly GCC_ARM32_PREFIX="$WORKSPACE_DIR/tools/arm_eabi_gcc/bin/arm-none-eabi-"

source "$SCRIPT_DIR/parse_params.sh"

readonly PLATFORM_OUT_DIR="$WORKSPACE_DIR/output/$PLATFORM"

for cmd in "${CMD[@]}" ; do
    cd "$WORKSPACE_DIR"
    do_$cmd
done
