#!/usr/bin/env bash

# Copyright (c) 2021-2022 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

initramfs_create() {
    echo "busybox: generate initramfs..." >&2
    # paths in initramfs.list are relative to WORKSPACE_DIR
    env -C "$WORKSPACE_DIR" \
    "$gen_init_cpio" "$SCRIPT_DIR/config/busybox/initramfs.list"
}

initramfs_extend() {
    local -r dir="$1" ; shift
    # check if there is any files except for README that should be added
    if [[ "$dir/README" != "$(echo "$dir/"*)" ]] ; then
        echo "busybox: extending initramfs..." >&2
        env -C "$dir/" find . -not -path "./README" -print0 \
        | env -C "$dir/" cpio --null --owner +0:+0 --create --format=newc
    fi
}

do_build()
{
    local -r gen_init_cpio="$WORKSPACE_DIR/linux/usr/gen_init_cpio"
    [[ -x "$gen_init_cpio" ]] ||
        die "Failed to locate kernel host executable artifact: $gen_init_cpio"
    cd "busybox"

    local -r busybox_cfg_dir="$SCRIPT_DIR/config/busybox"
    if [[ "$busybox_cfg_dir/config" -nt ".config" ]] ; then
        make \
            CROSS_COMPILE="$GCC_ARM64_PREFIX" \
            KBUILD_DEFCONFIG="$busybox_cfg_dir/config" \
            "-j$PARALLELISM" \
            defconfig
    fi
    make \
        CROSS_COMPILE="$GCC_ARM64_PREFIX" \
        "-j$PARALLELISM" \
        busybox

    {
        initramfs_create
        initramfs_extend "$SCRIPT_DIR/config/busybox/initramfs.files.d"
    } | esp_compress > "$PLATFORM_OUT_DIR/intermediates/busybox.initramfs"

    : > "$PLATFORM_OUT_DIR/intermediates/busybox.root.img"
    truncate \
        --size="${BUSYBOX_FS_EXT4_SIZE}M" \
        "$PLATFORM_OUT_DIR/intermediates/busybox.root.img"
    mkfs.ext4 "$PLATFORM_OUT_DIR/intermediates/busybox.root.img"
}

do_clean() {
    make -C "busybox" mrproper
    rm -f \
        "$PLATFORM_OUT_DIR/intermediates/busybox.initramfs" \
        "$PLATFORM_OUT_DIR/intermediates/busybox.root.img"
}

source "$(dirname ${BASH_SOURCE[0]})/framework.sh"
