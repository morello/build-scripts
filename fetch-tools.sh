#!/usr/bin/env bash

# Copyright (c) 2021-2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

# [data_url]     : URL to the tar to download and extract
# [checksum_url] : checksum URL. Checked during 'update' and if the contents
#                  have changed it will trigger a new download.
# [sanity_file]  : this file should exist after extraction
# [extra_tar_opt]: optional extra argument to tar
# [format]       : how to extract the downloaded file
readonly -A TOOL_clang=(
    [data_url]="https://git.morello-project.org/morello/llvm-project/-/jobs/artifacts/morello/master/raw/morello-clang.tar.xz?job=build-toolchain"
    [checksum_url]="https://git.morello-project.org/morello/llvm-project/-/jobs/artifacts/morello/master/raw/SHA256SUMS.txt?job=build-toolchain"
    [sanity_file]="bin/clang"
)
readonly -A TOOL_arm_linux_gcc=(
    [data_url]="https://developer.arm.com/-/media/Files/downloads/gnu/11.2-2022.02/binrel/gcc-arm-11.2-2022.02-x86_64-aarch64-none-linux-gnu.tar.xz"
    [sanity_file]="bin/aarch64-none-linux-gnu-gcc"
)
readonly -A TOOL_arm_eabi_gcc=(
    [data_url]="https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2020q2/gcc-arm-none-eabi-9-2020-q2-update-x86_64-linux.tar.bz2"
    [sanity_file]="bin/arm-none-eabi-gcc"
)
readonly -A TOOL_rtl_firmware=(
    [data_url]="https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/plain/rtl_nic/rtl8168g-2.fw?h=20210511"
    [sanity_file]="rtl8168g-2.fw"
    [format]="plain"
)
readonly -A TOOL_clang_linux_x86=(
    [data_url]="https://git.morello-project.org/morello/llvm-project/-/jobs/artifacts/morello/master/raw/llvm-morello-linux-x86-clang.tar.gz?job=build-musl-x86"
    [sanity_file]="bin/clang"
)

readonly TOOLS=(
    arm_eabi_gcc
    clang
    arm_linux_gcc
    rtl_firmware
    clang_linux_x86
)

readonly DO_DESC_build="download tools if not already downloaded"
do_build()
{
    mkdir -p "$WORKSPACE_DIR/tools"

    local tool
    for tool in "${TOOLS[@]}" ; do
        local -n tool_info="TOOL_$tool"
        local tool_dir="tools/$tool"
        if [[ -e "$tool_dir" ]] ; then
            echo "$tool: already extracted"
            continue
        fi

        local work_dir="$PLATFORM_OUT_DIR/intermediates/tools/$tool"
        rm -fr "$work_dir"
        mkdir -p "$work_dir"

        echo "$tool: downloading..."
        {
            echo "${tool_info[data_url]}"
            echo "${tool_info[checksum_url]:-}"
        } > "$work_dir/url"

        if [[ -n "${tool_info[checksum_url]:-}" ]] ; then
            wget -O "$work_dir/checksum" "${tool_info[checksum_url]}"
        fi

        local data_file="${tool_info[data_url]##*/}"
        data_file="${data_file%\?*}"
        wget -O "$work_dir/$data_file" "${tool_info[data_url]}"

        echo "$tool: extracting..."
        mkdir "$work_dir/extract"
        local format="${tool_info[format]:-tar}"
        case "${tool_info[format]:-tar}" in
        ("plain")
            mv "$work_dir/$data_file" "$work_dir/extract/"
            ;;
        ("tar")
            if ! tar --no-same-owner --no-same-permissions --strip-components=1 ${tool_info[extra_tar_opt]-} -C "$work_dir/extract" -xf "$work_dir/$data_file" ; then
                error_echo "$tool: extraction failed!"
                error_echo "$tool: keeping work directory: $work_dir"
                exit 1
            fi
            ;;
        (*)
            die "Tool \"$tool\" have an unknown file format defined: $format"
        esac
        local sanity_file="${tool_info[sanity_file]}"
        if ! [[ -e "$work_dir/extract/$sanity_file" ]] ; then
            error_echo "$tool: sanity check failed!"
            error_echo "$tool: file does not exist: ${tool_info[sanity_file]}"
            error_echo "$tool: keeping work directory: $work_dir"
            exit 1
        fi
        mv "$work_dir/url" "tools/.$tool.url"
        if [[ -e "$work_dir/checksum" ]] ; then
            mv "$work_dir/checksum" "tools/.$tool.checksum"
        fi
        mv "$work_dir/extract" "$tool_dir"
        rm -r "$work_dir"
        rmdir --ignore-fail-on-non-empty "$PLATFORM_OUT_DIR/intermediates/tools"
        echo "$tool: done"
    done
}

do_clean() {
    return 0
}

fetchtools_clear() {
    local tool="$1" ; shift
    local tool_dir="tools/$tool"
    echo "$tool: '$tool_dir' -> '$tool_dir.old'"
    rm -rf "$tool_dir.old"
    mv "$tool_dir" "$tool_dir.old"
}

# check for every tool for URL changes or if the remote checksum file has been
# updated and if so trigger a new download of that tool
readonly DO_DESC_update="check for updated tools and download if needed"
do_update() {
    # clear any outdated tools
    local tool
    for tool in "${TOOLS[@]}" ; do
        local -n tool_info="TOOL_$tool"
        local url_file="tools/.$tool.url"
        local checksum_file="tools/.$tool.checksum"

        local tool_dir="tools/$tool"
        [[ -e "$tool_dir" ]] || continue

        if ! [[ -e "$url_file" ]] ; then
            echo "$tool: missing '$url_file', assuming manual user setup"
            continue
        fi

        mapfile -t old_url < "$url_file"
        if [[ "${tool_info[data_url]}" != "${old_url[0]:-}" ]] ; then
            echo "$tool: data URL changed"
            fetchtools_clear "$tool"
            continue
        fi
        if [[ "${tool_info[checksum_url]:-}" != "${old_url[1]:-}" ]] ; then
            echo "$tool: checksum URL changed"
            fetchtools_clear "$tool"
            continue
        fi

        if [[ -n "${tool_info[checksum_url]:-}" ]] ; then
            echo "$tool: fetching checksum file..."
            # currently any type of fetch failure causes us to skip the check
            if wget -O "$PLATFORM_OUT_DIR/intermediates/$tool.checksum" "${tool_info[checksum_url]}" ; then
                if cmp --silent "$PLATFORM_OUT_DIR/intermediates/$tool.checksum" "$checksum_file" ; then
                    echo "$tool: no updated checksum file"
                else
                    echo "$tool: updated checksum file detected!"
                    fetchtools_clear "$tool"
                fi
                rm -f "$PLATFORM_OUT_DIR/intermediates/$tool.checksum"
            else
                echo -e "$tool: failed to fetch checksum file from remote, ${BOLD}skipping check$NORMAL."
            fi
        fi
    done

    # download the new version of anything removed
    do_build
}

source "$(dirname ${BASH_SOURCE[0]})/framework.sh"
