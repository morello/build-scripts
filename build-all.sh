#!/usr/bin/env bash

# Copyright (c) 2021-2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

for_each_build_script() {
    local scripts=(
        "fetch-tools.sh"
        "build-scp.sh"
        "build-arm-tf.sh"
        "build-uefi.sh"
        "build-firmware-image.sh"
    )

    case "$FILESYSTEM" in
    ("busybox")
        scripts+=(
            "build-grub.sh"
            "build-linux.sh"
            "build-busybox.sh"
            "build-disk-image.sh"
        )
        ;;
    ("android-"*)
        scripts+=(
            "build-grub.sh"
            "build-linux.sh"
            "build-android.sh"
            "build-disk-image.sh"
        )
        ;;
    ("none")
         ;;
    (*) false
    esac

    local script
    for script in "${scripts[@]}" ; do
        "$SCRIPT_DIR/$script" -p "$PLATFORM" -f "$FILESYSTEM" "$@" || exit 1
    done
}

do_build()
{
    for_each_build_script build
}

do_clean() {
    for_each_build_script clean
}

source "$(dirname ${BASH_SOURCE[0]})/framework.sh"
