#!/usr/bin/env bash

# Copyright (c) 2021-2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

do_build()
{
    fw_version=$(fetch_fw_version "$WORKSPACE_DIR/bsp/uefi/edk2" "EDK2")
    fw_version_plat=$(fetch_fw_version "$WORKSPACE_DIR/bsp/uefi/edk2/edk2-platforms" "EDK2-Platforms")
    local make_opts=(
        -a AARCH64
        -D FIRMWARE_VER="$fw_version:$fw_version_plat"
        -s
        -p "edk2-platforms/Platform/ARM/Morello/MorelloPlatform${PLATFORM^}.dsc"
    )
    local artifact_dir="$WORKSPACE_DIR/bsp/uefi/edk2/Build/morello$PLATFORM/"

    case "${UEFI_BUILD_MODE^^}" in
    ("DEBUG"|"RELEASE")
        make_opts+=( -b "${UEFI_BUILD_MODE^^}" )
        artifact_dir+="${UEFI_BUILD_MODE^^}"
        ;;
    (*) die "Unsupported value for UEFI_BUILD_MODE: $UEFI_BUILD_MODE"
    esac
    artifact_dir+="_"

    case "$UEFI_MORELLO_CAPABILITIES" in
    ("1") make_opts+=(-D ENABLE_MORELLO_CAP) ;;
    ("0") ;;
    (*)   die "Unsupported value for UEFI_MORELLO_CAPABILITIES: $UEFI_MORELLO_CAPABILITIES"
    esac

    case "${UEFI_TOOLCHAIN^^}" in
    ("LLVM")
        make_opts+=( -t CLANGDWARF )
        artifact_dir+="CLANGDWARF"
        # otherwise clang will try to use system ld
        PATH="$(dirname ${GCC_ARM64_PREFIX}ld):$PATH"
        ;;
    ("GNU")
        make_opts+=( -t GCC5 )
        artifact_dir+="GCC5"
        ;;
    (*)
        echo "Bad UEFI_TOOLCHAIN value: $UEFI_TOOLCHAIN" >&2
        return 1
    esac
    artifact_dir+="/FV"

    export CLANGDWARF_BIN="$LLVM_PATH/"
    export GCC5_AARCH64_PREFIX="$GCC_ARM64_PREFIX"
    export IASL_PREFIX="$WORKSPACE_DIR/deps/acpica/generate/unix/bin/"
    export PACKAGES_PATH="$WORKSPACE_DIR/bsp/uefi/edk2/edk2-platforms:$WORKSPACE_DIR/bsp/uefi/edk2-non-osi"
    export PYTHON_COMMAND="python3"

    make -C "deps/acpica" "-j$PARALLELISM" iasl
    make -C "bsp/uefi/edk2/BaseTools" "-j$PARALLELISM"

    cd "bsp/uefi/edk2"
    with_default_shell_opts source ./edksetup.sh --reconfig
    BaseTools/BinWrappers/PosixLike/build "${make_opts[@]}"

    mkdir -p "$PLATFORM_OUT_DIR/intermediates"
    cp "$artifact_dir/BL33_AP_UEFI.fd" "$PLATFORM_OUT_DIR/intermediates/uefi.bin"
}

do_clean()
{
    rm -rf \
        "bsp/uefi/edk2/Build" \
        "bsp/uefi/edk2/Conf/BuildEnv.sh" \
        "$PLATFORM_OUT_DIR/intermediates/uefi.bin" \

    make --no-print-directory -C "deps/acpica" veryclean
    make --no-print-directory -C "bsp/uefi/edk2/BaseTools" clean
}

source "$(dirname ${BASH_SOURCE[0]})/framework.sh"
