#!/usr/bin/env bash

# Copyright (c) 2021-2022 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

readonly GRUB_MODULES=(
    boot
    chain
    configfile
    ext2
    fat
    gzio
    help
    linux
    loadenv
    lsefi
    normal
    ntfs
    ntfscomp
    part_gpt
    part_msdos
    progress
    read
    search
    search_fs_file
    search_fs_uuid
    search_label
    terminal
    terminfo
)

do_build()
{
    local -r grub_work_dir="$PLATFORM_OUT_DIR/intermediates/grub"
    local -r state_file="$grub_work_dir/grub.state.configured"
    mkdir -p "$grub_work_dir/build"
    cd "$grub_work_dir/build"

    if newer_ctime "$state_file" \
        "$SCRIPT_DIR/build-grub.sh" \
        "$SCRIPT_DIR/framework.sh" \
        "$WORKSPACE_DIR/deps/gnulib" \
        "$WORKSPACE_DIR/grub" \
    ; then
        env \
            -C "$WORKSPACE_DIR/grub" \
            PYTHON="python3" \
            GNULIB_SRCDIR="$WORKSPACE_DIR/deps/gnulib" \
            ./bootstrap

        "$WORKSPACE_DIR/grub/configure" \
            TARGET_CC="${GCC_ARM64_PREFIX}gcc" \
            TARGET_OBJCOPY="${GCC_ARM64_PREFIX}objcopy" \
            TARGET_STRIP="${GCC_ARM64_PREFIX}strip" \
            --target=aarch64-linux-gnu \
            --prefix="$grub_work_dir/install" \
            --with-platform=efi \
            --enable-dependency-tracking \
            --disable-efiemu \
            --disable-werror \
            --disable-grub-mkfont \
            --disable-grub-themes \
            --disable-grub-mount
    else
        echo "grub: skipping bootstrap/configure"
    fi

    make --no-print-directory "-j$PARALLELISM" install

    echo 'set prefix=($root)/grub/' > "$grub_work_dir/embedded.cfg"
    "$grub_work_dir/install/bin/grub-mkimage" \
        -c "$grub_work_dir/embedded.cfg" \
        -o "$PLATFORM_OUT_DIR/intermediates/grub.efi" \
        -O arm64-efi \
        -p "" \
        "${GRUB_MODULES[@]}"

    touch "$state_file"
}

do_clean() {
    rm -rf \
        "$PLATFORM_OUT_DIR/intermediates/grub.efi" \
        "$PLATFORM_OUT_DIR/intermediates/grub/" \

}

source "$(dirname ${BASH_SOURCE[0]})/framework.sh"
