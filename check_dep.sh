#!/usr/bin/env bash

# Copyright (c) 2021-2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

# this tool check if required host packages have been installed on support
# systems.
#
# exit code 0  - no dependency missing
# exit code !0 - dependency missing or unable to check

set -e
set -u

DEPS_ubuntu_focal=(
    "autoconf"
    "autopoint"
    "bc"
    "bison"
    "build-essential"
    "cpio"
    "curl"
    "device-tree-compiler"
    "dosfstools"
    "doxygen"
    "fdisk"
    "flex"
    "gdisk"
    "gettext-base"
    "git"
    "libncurses5"
    "libssl-dev"
    "libtinfo5"
    "linux-libc-dev-arm64-cross"
    "m4"
    "mtools"
    "pkg-config"
    "python-is-python3"
    "python3-distutils"
    "rsync"
    "snapd"
    "unzip"
    "uuid-dev"
    "wget"
)

if test "${1-}" = "fvp" ; then
    DEPS_ubuntu_bionic+=(
        "telnet"
        "xterm"
    )
fi

if ! command -v lsb_release ; then
    echo "ERROR: Unable to check dependencies due to missing command 'lsb_release'!" >&2
    exit 1
fi

readonly DIST_INFO=( $(lsb_release -ics) )
readonly DISTRIBUTION="${DIST_INFO[0],,}"
readonly CODENAME="${DIST_INFO[1],,}"
case "$DISTRIBUTION" in
("ubuntu")
    deps="DEPS_ubuntu_$CODENAME"
    if ! [[ -v "$deps" ]] ; then
        echo "ERROR: Unknown Ubuntu: $CODENAME" >&2
        exit 1
    fi
    deps="$deps[@]"
    for dep in "${!deps}" ; do
        if ! LC_ALL=C dpkg-query --show -f='${Status}\n' "$dep" 2>/dev/null  | grep -qE '([[:blank:]]|^)installed([[:blank:]]|$)' ; then
            echo "$dep"
        fi
    done \
    | sort \
    | {
        mapfile -t missing_deps
        if [[ "${#missing_deps[@]}" -ne 0 ]] ; then
            echo "The following packages was detected as missing:"
            for s in "${missing_deps[@]}" ; do
                echo "  * $s"
            done
            echo
            echo "Install them with this commands:"
            echo "sudo apt-get install" "${missing_deps[@]}"
            exit 1
        fi
    }
    ;;
(*)
    echo "ERROR: Unknown distribution, can not check dependencies!" >&2
    exit 1
esac
echo "no missing dependencies detected."
