#!/usr/bin/env bash

# Copyright (c) 2021-2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

do_build()
{
    "bsp/arm-tf/tools/fiptool/fiptool" create \
        --scp-fw "$PLATFORM_OUT_DIR/intermediates/scp-ram.bin" \
        "$PLATFORM_OUT_DIR/intermediates/scp_fw.bin"

    "bsp/arm-tf/tools/fiptool/fiptool" create \
        --blob uuid=54464222-a4cf-4bf8-b1b6-cee7dade539e,file="$PLATFORM_OUT_DIR/intermediates/mcp-ram.bin" \
        "$PLATFORM_OUT_DIR/intermediates/mcp_fw.bin"

    local fip_tool_opts=(
        --tb-fw "$PLATFORM_OUT_DIR/intermediates/tf-bl2.bin"
        --nt-fw "$PLATFORM_OUT_DIR/intermediates/uefi.bin"
        --soc-fw "$PLATFORM_OUT_DIR/intermediates/tf-bl31.bin"
        --fw-config "$PLATFORM_OUT_DIR/intermediates/morello_fw_config.dtb"
        --hw-config "$PLATFORM_OUT_DIR/intermediates/morello.dtb"
        --tb-fw-config "$PLATFORM_OUT_DIR/intermediates/morello_tb_fw_config.dtb"
        --nt-fw-config "$PLATFORM_OUT_DIR/intermediates/morello_nt_fw_config.dtb"
        --trusted-key-cert "$PLATFORM_OUT_DIR/intermediates/tfa_certs/trusted_key.crt"
        --soc-fw-key-cert "$PLATFORM_OUT_DIR/intermediates/tfa_certs/bl31_key.crt"
        --nt-fw-key-cert "$PLATFORM_OUT_DIR/intermediates/tfa_certs/bl33_key.crt"
        --soc-fw-cert "$PLATFORM_OUT_DIR/intermediates/tfa_certs/bl31.crt"
        --nt-fw-cert "$PLATFORM_OUT_DIR/intermediates/tfa_certs/bl33.crt"
        --tb-fw-cert "$PLATFORM_OUT_DIR/intermediates/tfa_certs/bl2.crt"
    )

    mkdir -p "$PLATFORM_OUT_DIR/intermediates/tfa_certs"
    "bsp/arm-tf/tools/cert_create/cert_create" "${fip_tool_opts[@]}" -n --tfw-nvctr 0 --ntfw-nvctr 0 \
        --rot-key "bsp/arm-tf/plat/arm/board/common/rotpk/arm_rotprivk_rsa.pem"

    "bsp/arm-tf/tools/fiptool/fiptool" create "${fip_tool_opts[@]}" "$PLATFORM_OUT_DIR/intermediates/fip.bin"

    mkdir -p "$PLATFORM_OUT_DIR/firmware"
    install --mode=644 "$PLATFORM_OUT_DIR/intermediates/scp_fw.bin"   "$PLATFORM_OUT_DIR/firmware/scp_fw.bin"
    install --mode=644 "$PLATFORM_OUT_DIR/intermediates/mcp_fw.bin"   "$PLATFORM_OUT_DIR/firmware/mcp_fw.bin"
    install --mode=644 "$PLATFORM_OUT_DIR/intermediates/fip.bin"      "$PLATFORM_OUT_DIR/firmware/fip.bin"
}

do_clean() {
    rm -f \
        "$PLATFORM_OUT_DIR/intermediates/mcp_fw.bin" \
        "$PLATFORM_OUT_DIR/intermediates/scp_fw.bin" \
        "$PLATFORM_OUT_DIR/intermediates/fip.bin" \
        "$PLATFORM_OUT_DIR/firmware/mcp_fw.bin" \
        "$PLATFORM_OUT_DIR/firmware/scp_fw.bin" \
        "$PLATFORM_OUT_DIR/firmware/fip.bin"

    rm -rf "${PLATFORM_OUT_DIR}/intermediates/tfa_certs"

    if [[ -e "$PLATFORM_OUT_DIR/firmware" ]] ; then
        rmdir --ignore-fail-on-non-empty "$PLATFORM_OUT_DIR/firmware"
    fi
}

source "$(dirname ${BASH_SOURCE[0]})/framework.sh"
