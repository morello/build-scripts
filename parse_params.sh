#!/usr/bin/env bash

# Copyright (c) 2021-2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

readonly FILESYSTEM_OPTIONS=(
    "none"
    "busybox"
    "android-nano"
    "android-nano_libshim"
    "android-nano_nolibshim"
    "android-swr"
)

readonly PLATFORM_DEFAULT="fvp"
readonly PLATFORM_OPTIONS=(
    "fvp"
    "soc"
)

readonly CMD_DEFAULT=( "build" )
readonly CMD_OPTIONS=( $(compgen -A function | sed -rne 's#^do_##p') )

source "$SCRIPT_DIR/config/bsp"

if [[ $ARM_TF_MORELLO_CAPABILITIES -eq 1 && ${ARM_TF_TOOLCHAIN^^} != "LLVM" ]] ; then
    die "For ARM_TF_MORELLO_CAPABILITIES == \"1\" requires ARM_TF_TOOLCHAIN == \"llvm\"."
fi
if [[ $LINUX_MORELLO_CAPABILITIES -eq 1 && ${LINUX_TOOLCHAIN^^} != "LLVM" ]] ; then
    die "For LINUX_MORELLO_CAPABILITIES == \"1\" requires LINUX_TOOLCHAIN == \"llvm\"."
fi
if [[ $UEFI_MORELLO_CAPABILITIES = 1 && ${UEFI_TOOLCHAIN^^} != "LLVM" ]] ; then
    die "For UEFI_MORELLO_CAPABILITIES == \"1\" requires UEFI_TOOLCHAIN == \"llvm\"."
fi
if [[ $ANDROID_MORELLO_CAPABILITIES != 1 ]] ; then
    die "ANDROID_MORELLO_CAPABILITIES != \"1\" not supported."
fi

print_usage() {
    echo -e "${BOLD}Usage:"
    echo -e "    $0 ${CYAN}-f FILESYSTEM [-p PLATFORM] [CMD...]$NORMAL"
    echo
    echo "FILESYSTEM:"
    local s
    for s in "${FILESYSTEM_OPTIONS[@]}" ; do
        echo "    $s"
    done
    echo
    echo "PLATFORM (default is \"$PLATFORM_DEFAULT\"):"
    for s in "${PLATFORM_OPTIONS[@]}" ; do
        echo "    $s"
    done
    echo
    echo "CMD (default is \"${CMD_DEFAULT[@]}\"):"
    local s_maxlen="0"
    for s in "${CMD_OPTIONS[@]}" ; do
        local i="${#s}"
        (( i > s_maxlen )) && s_maxlen="$i"
    done
    for s in "${CMD_OPTIONS[@]}" ; do
        local -n desc="DO_DESC_$s"
        printf "    %- ${s_maxlen}s    %s\n" "$s" "${desc:+($desc)}"
    done
}

PLATFORM="$PLATFORM_DEFAULT"
FILESYSTEM=""
CMD=( "${CMD_DEFAULT[@]}" )
while getopts "p:f:h" opt; do
    case $opt in
    ("p") PLATFORM="$OPTARG" ;;
    ("f") FILESYSTEM="$OPTARG" ;;
    ("?")
        print_usage >&2
        exit 1
        ;;
    ("h")
        print_usage
        exit 0
    esac
done
shift $((OPTIND-1))

in_haystack "$PLATFORM" "${PLATFORM_OPTIONS[@]}" ||
    die "invalid PLATFORM: $PLATFORM"
readonly PLATFORM

if [[ -z "${FILESYSTEM:-}" ]] ; then
    echo "ERROR: Mandatory -f FILESYSTEM not given!" >&2
    echo "" >&2
    print_usage >&2
    exit 1
fi
in_haystack "$FILESYSTEM" "${FILESYSTEM_OPTIONS[@]}" ||
    die "invalid FILESYSTEM: $FILESYSTEM"
readonly FILESYSTEM

if [[ "$#" -ne 0 ]] ; then
    CMD=( "$@" )
fi
for cmd in "${CMD[@]}" ; do
    in_haystack "$cmd" "${CMD_OPTIONS[@]}" || die "invalid CMD: $cmd"
done
readonly CMD
