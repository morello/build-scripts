#!/bin/busybox sh

# Copyright (c) 2021-2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

mount() {
    /bin/busybox mount "$@"
}

grep() {
    /bin/busybox grep "$@"
}

mount -t proc proc /proc
grep -qE $'\t'"devtmpfs$" /proc/filesystems && mount -t devtmpfs dev /dev
mount -t sysfs sysfs /sys

/bin/busybox --install -s

! grep -qE $'\t'"devtmpfs$" /proc/filesystems && mdev -s

for s in /init.sh.d/*.sh ; do
    test -e "$s" && . "$s"
done

echo "init.sh"
exec /sbin/init
