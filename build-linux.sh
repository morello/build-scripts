#!/usr/bin/env bash

# Copyright (c) 2021-2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

do_build()
{
    cd "linux"

    local make_opts=(
        ARCH=arm64
        "-j$PARALLELISM"
    )

    case "$LINUX_MORELLO_CAPABILITIES" in
    ("0") CAPABILITIES_STR="disable" ;;
    ("1") CAPABILITIES_STR="enable"  ;;
    (*)   die "Bad LINUX_MORELLO_CAPABILITIES value: $LINUX_MORELLO_CAPABILITIES"
    esac

    local firmware_dir="$PLATFORM_OUT_DIR/intermediates/linux_firmware"
    rm -rf "$firmware_dir"
    mkdir -p "$firmware_dir/rtl_nic/"
    cp --archive --reflink=auto --target-directory="$firmware_dir/rtl_nic/" \
        "$WORKSPACE_DIR/tools/rtl_firmware/rtl8168g-2.fw" \

    local firmware_files="$(env -C "$firmware_dir" find . -not -type d | sed -re 's#^..##' | tr '\n' ' ')"

    case "${LINUX_TOOLCHAIN^^}" in
    ("LLVM")
            PATH="$LLVM_PATH:$PATH"
            make_opts+=(
                CROSS_COMPILE="aarch64-none-linux-gnu-"
                LLVM=1
                LLVM_IAS=1
            )
        ;;
    ("GNU")
        make_opts+=(
            CROSS_COMPILE="$GCC_ARM64_PREFIX"
        )
        ;;
    (*)
        echo "Bad LINUX_TOOLCHAIN value: $LINUX_TOOLCHAIN" >&2
        return 1
    esac

    local kernel_config=(
        --$CAPABILITIES_STR ARM64_MORELLO
        --enable ANON_VMA_NAME

        # our platform have ACPI
        --enable ACPI

        # otherwise crash in UEFI-stub
        --disable SHADOW_CALL_STACK

        # otherwise not all RAM is available
        --disable ARM64_VA_BITS_39
        --enable ARM64_VA_BITS_48

        # for facilitating mailbox communication (AP kernel <-> SCP), mainly for DVFS
        --enable ARM_MHU
        --enable PLATFORM_MHU

        # for BusyBox and Android Nano
        --enable IP_PNP
        --enable IP_PNP_DHCP

        # DVFS
        --enable ARM_SCMI_PROTOCOL
        --enable ARM_SCMI_POWER_DOMAIN
        --enable ARM_SCMI_CPUFREQ
        --enable SENSORS_ARM_SCMI
        --enable COMMON_CLK_SCMI
        --enable CPU_FREQ_GOV_POWERSAVE

        --enable ARM_SMMU_V3

        --set-str EXTRA_FIRMWARE "$firmware_files"
        --set-str EXTRA_FIRMWARE_DIR "$firmware_dir"

        --enable USB_XHCI_PCI
        --enable USB_XHCI_PCI_RENESAS
        --enable USB_STORAGE

        --enable DRM
        --enable DRM_FBDEV_EMULATION
        --enable DRM_I2C_NXP_TDA998X
        --enable DRM_KOMEDA
        --enable DRM_PANFROST
        --enable FB_EFI
        --enable I2C_CADENCE

        --enable ATA
        --enable SATA_AHCI
        --enable BLK_DEV_NVME

        --enable ETHERNET
        --enable R8169

        # FVP only hardware
        --enable SMC91X
        --enable VIRTIO_BLK
        --enable VIRTIO_MMIO
        --enable VIRTIO_NET
        --enable HW_RANDOM
        --enable HW_RANDOM_VIRTIO
        # this enables directory sharing between FVP and host OS
        --enable NET_9P
        --enable NET_9P_VIRTIO
        --enable 9P_FS
        # fix "NOHZ tick-stop error" warnings on 5.10 kernel
        --enable RT_SOFTINT_OPTIMIZATION
        # Sound support
        --enable SND_SOC_XILINX_I2S
        --enable SND_SOC_XILINX_AUDIO_FORMATTER
        --enable SND_SIMPLE_CARD

        --disable COMPAT
    )

    case "$FILESYSTEM" in
    ("android-"*) # android-swr_nolibshim is unsupported for now
        kernel_config+=(
            --disable LTO_CLANG_FULL
            --enable LTO_CLANG_THIN

            --disable IKHEADERS
            --disable HEADERS_INSTALL
            --disable KASAN

            # remove pKVM and KASAN arguments
            --set-str CMDLINE ""
        )
        ;;& # attempt another match
    ("android-nano_nolibshim")
        kernel_config+=(
            --disable BLK_DEV_NVME # not yet supported by PCuABI kernel (https://git.morello-project.org/morello/kernel/linux/-/issues/56)
            --enable COMPAT # to be enabled when CONFIG_CHERI_PURECAP_ABI is enabled
        )
        defconfig="morello_transitional_pcuabi_defconfig" ;;
    (*) defconfig="defconfig" ;;
    esac
    local -r kernel_config
    mkdir -p "$PLATFORM_OUT_DIR/intermediates"

    # avoid relinking due to timestamp on .config when its contents didn't
    # change
    export KCONFIG_CONFIG="$PLATFORM_OUT_DIR/intermediates/kernel_config_tmp"
    case "$FILESYSTEM" in
    ("android-nano"|"android-nano_libshim"|"android-swr")
        ./scripts/kconfig/merge_config.sh -m \
            "arch/arm64/configs/defconfig" \
            "$SCRIPT_DIR/config/android/android-base.config" \
            "$SCRIPT_DIR/config/android/android-recommended.config"
        ;;
    (*) make "${make_opts[@]}" "$defconfig" ;; # android-nano_nolibshim, busybox
    esac
    ./scripts/config --file "$KCONFIG_CONFIG" "${kernel_config[@]}"
    make "${make_opts[@]}" olddefconfig
    if ! cmp --silent "$KCONFIG_CONFIG" ".config" ; then
        cp "$KCONFIG_CONFIG" ".config"
    fi
    rm -f "$KCONFIG_CONFIG" "$KCONFIG_CONFIG.old"
    unset KCONFIG_CONFIG

    make "${make_opts[@]}" Image

    esp_compress \
        < "arch/arm64/boot/Image" \
        > "$PLATFORM_OUT_DIR/intermediates/kernel_$FILESYSTEM"
}

do_clean() {
    make -C "linux" distclean

    rm -fr \
        "$PLATFORM_OUT_DIR/intermediates/kernel_$FILESYSTEM" \
        "$PLATFORM_OUT_DIR/intermediates/linux_firmware" \

}

source "$(dirname ${BASH_SOURCE[0]})/framework.sh"
