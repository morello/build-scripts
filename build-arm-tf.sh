#!/usr/bin/env bash

# Copyright (c) 2021-2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

do_build()
{
    fw_version=$(fetch_fw_version "$WORKSPACE_DIR/bsp/arm-tf" "TF-A")
    readonly OPENSSL_SRC="$WORKSPACE_DIR/bsp/deps/openssl"
    readonly OPENSSL_PATH="$PLATFORM_OUT_DIR/intermediates/host_openssl"
    local -r state_file="$OPENSSL_PATH/host.openssl.state"

    if newer_ctime "$state_file" \
        "$OPENSSL_SRC" \
        "$SCRIPT_DIR/build-arm-tf.sh" \
        "$SCRIPT_DIR/framework.sh" \
    ; then
        rm -rf "$OPENSSL_PATH"
        mkdir -p "$OPENSSL_PATH/build"
        cd "$OPENSSL_PATH/build"
        $OPENSSL_SRC/Configure \
            --libdir=lib \
            --prefix="$OPENSSL_PATH/install" \
            --api=1.0.1 \
            no-shared \
            no-dso \
            no-threads
        make -j$PARALLELISM
        make -j$PARALLELISM install_sw
        cd -
        touch "$state_file"
        echo "OpenSSL Installed"
    fi

    make OPENSSL_DIR="$OPENSSL_PATH/install" --no-print-directory -C "bsp/arm-tf/tools/fiptool"
    make OPENSSL_DIR="$OPENSSL_PATH/install" --no-print-directory -C "bsp/arm-tf/tools/cert_create"

    local make_opts=(
        --no-print-directory
        -C "bsp/arm-tf"
        -j "$PARALLELISM"
        ARCH=aarch64
        E=0
        PLAT=morello
        ARM_ROTPK_LOCATION="devel_rsa"
        CREATE_KEYS=1
        GENERATE_COT=1
        MBEDTLS_DIR="$WORKSPACE_DIR/bsp/deps/mbedtls"
        ROT_KEY="plat/arm/board/common/rotpk/arm_rotprivk_rsa.pem"
        TRUSTED_BOARD_BOOT=1
        VERSION_STRING="$fw_version"
        all
    )

    case "$PLATFORM" in
    ("soc") make_opts+=( "TARGET_PLATFORM=soc") ;;
    ("fvp") make_opts+=( "TARGET_PLATFORM=fvp") ;;
    (*) die "unexpected PLATFORM" ;;
    esac

    artifact_dir="bsp/arm-tf/build/morello/"
    case "${ARM_TF_BUILD_MODE^^}" in
    ("DEBUG")
        make_opts+=("DEBUG=1")
        artifact_dir+="debug"
        ;;
    ("RELEASE")
        make_opts+=("DEBUG=0")
        artifact_dir+="release"
        ;;
    (*) die "Unsupported value for ARM_TF_BUILD_MODE: $ARM_TF_BUILD_MODE"
    esac

    case "${ARM_TF_TOOLCHAIN^^}" in
    ("LLVM")
        make_opts+=(
            CC="$LLVM_PATH/clang"
            LD="$LLVM_PATH/ld.lld"
            CROSS_COMPILE="$LLVM_PATH/llvm-"
        )
        ;;
    ("GNU")
        make_opts+=(
            CROSS_COMPILE="$GCC_ARM64_PREFIX"
        )
        ;;
    (*) die "Unsupported value for ARM_TF_TOOLCHAIN: $ARM_TF_TOOLCHAIN"
    esac

    case "$ARM_TF_MORELLO_CAPABILITIES" in
    ("1"|"0")
        make_opts+=(" ENABLE_MORELLO_CAP=$ARM_TF_MORELLO_CAPABILITIES" )
        ;;
    (*) die "Unsupported value for ARM_TF_MORELLO_CAPABILITIES: $ARM_TF_MORELLO_CAPABILITIES"
    esac

    make "${make_opts[@]}"

    mkdir -p "$PLATFORM_OUT_DIR/intermediates"
    cp "$artifact_dir/bl2.bin"                       "$PLATFORM_OUT_DIR/intermediates/tf-bl2.bin"
    cp "$artifact_dir/bl31.bin"                      "$PLATFORM_OUT_DIR/intermediates/tf-bl31.bin"
    cp "$artifact_dir/fdts/morello-$PLATFORM.dtb"    "$PLATFORM_OUT_DIR/intermediates/morello.dtb"
    cp "$artifact_dir/fdts/morello_fw_config.dtb"    "$PLATFORM_OUT_DIR/intermediates/morello_fw_config.dtb"
    cp "$artifact_dir/fdts/morello_tb_fw_config.dtb" "$PLATFORM_OUT_DIR/intermediates/morello_tb_fw_config.dtb"
    cp "$artifact_dir/fdts/morello_nt_fw_config.dtb" "$PLATFORM_OUT_DIR/intermediates/morello_nt_fw_config.dtb"
}

do_clean()
{
    rm -rf \
        "$PLATFORM_OUT_DIR/intermediates/morello.dtb" \
        "$PLATFORM_OUT_DIR/intermediates/morello_fw_config.dtb" \
        "$PLATFORM_OUT_DIR/intermediates/morello_nt_fw_config.dtb" \
        "$PLATFORM_OUT_DIR/intermediates/morello_tb_fw_config.dtb" \
        "$PLATFORM_OUT_DIR/intermediates/tf-bl2.bin" \
        "$PLATFORM_OUT_DIR/intermediates/tf-bl31.bin" \

    local make_clean_opts=(
        --no-print-directory
        ENABLE_MORELLO_CAP=$ARM_TF_MORELLO_CAPABILITIES
        -C "bsp/arm-tf"
        distclean
    )

    if [ $ARM_TF_MORELLO_CAPABILITIES -eq 1 ] ; then
        make_clean_opts+=(
            CC="$LLVM_PATH/clang"
        )
    fi

    make "${make_clean_opts[@]}"
}

source "$(dirname ${BASH_SOURCE[0]})/framework.sh"
