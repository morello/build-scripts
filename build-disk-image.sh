#!/usr/bin/env bash

# Copyright (c) 2021-2023 Arm Limited. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause

readonly ESP_SIZE="AUTO"

do_build() {
    local grub_cfg
    local initramfs
    local -a partitions
    case "$FILESYSTEM" in
    ("android-"*)
        grub_cfg="$SCRIPT_DIR/config/android/grub_$PLATFORM.cfg"
        initramfs="$WORKSPACE_DIR/android/out/target/product/morello/ramdisk.img"
        # make sure to synchronise change of partition names with the Android fstab
        partitions=(
            ":morello-android-root:$WORKSPACE_DIR/android/out/target/product/morello/system.img"
            ":morello-android-data:$WORKSPACE_DIR/android/out/target/product/morello/userdata.img"
        )
        ;;
    (*)
        grub_cfg="$SCRIPT_DIR/config/$FILESYSTEM/grub_$PLATFORM.cfg"
        initramfs="$PLATFORM_OUT_DIR/intermediates/$FILESYSTEM.initramfs"
        partitions=(
            "::$PLATFORM_OUT_DIR/intermediates/$FILESYSTEM.root.img"
        )
        ;;
    esac

    "$SCRIPT_DIR/tools/mk-part-fat" \
        -o "$PLATFORM_OUT_DIR/intermediates/$FILESYSTEM.esp.img" \
        -s "$ESP_SIZE" \
        -l "ESP" \
        "$PLATFORM_OUT_DIR/intermediates/grub.efi"           "/EFI/BOOT/BOOTAA64.EFI" \
        "$grub_cfg"                                          "/grub/grub.cfg" \
        "$PLATFORM_OUT_DIR/intermediates/kernel_$FILESYSTEM" "/Image" \
        "$initramfs"                                         "/initramfs"

    "$SCRIPT_DIR/tools/mk-disk-gpt" \
        -o "$PLATFORM_OUT_DIR/intermediates/$FILESYSTEM.img" \
        "ESP::$PLATFORM_OUT_DIR/intermediates/$FILESYSTEM.esp.img" \
        "${partitions[@]}"

    mv "$PLATFORM_OUT_DIR/intermediates/$FILESYSTEM.img" "$PLATFORM_OUT_DIR/"
}

do_clean() {
    rm -f \
        "$PLATFORM_OUT_DIR/intermediates/$FILESYSTEM.esp.img" \
        "$PLATFORM_OUT_DIR/intermediates/$FILESYSTEM.img" \
        "$PLATFORM_OUT_DIR/$FILESYSTEM.img" \

}

source "$(dirname ${BASH_SOURCE[0]})/framework.sh"
